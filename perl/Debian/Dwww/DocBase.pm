# vim:ft=perl:cindent:ts=4:et

package Debian::Dwww::DocBase;

use Exporter();
use Debian::Dwww::Version;
use strict;

use vars qw(@ISA @EXPORT $ErrorProc);
@ISA    = qw(Exporter);
@EXPORT = qw(ParseDocBaseFile DwwwSection2Section $ErrorProc);



sub ParseDocBaseFile {
    my $file    = shift;
    my $format  = undef;
    my $entry   = {};
    my $last = undef;
    my $line  = 0;
    local $_;

    if (not open DOCFILE, $file) {
        &$ErrorProc($file, "Can't be opened: $!");
        return undef;
    }

    while (<DOCFILE>) {
        chomp;
        s/\s+$//;
        $line++;

        if (/^\s*$/) {
            # empty lines separate sections
            $format  = '';  # here we define $format
            $last = undef;
        } elsif (/^(\S+)\s*:\s*(.*)\s*$/) {
            my ($fld, $val) = (lc $1, $2);
            $last = undef;

            if (not defined $format) {
                $last = \$entry->{$fld};
            } elsif ($format eq '' && $fld eq 'format') {
                $format = lc $val;
            } elsif ($format ne '' && ($fld eq 'index' || $fld eq 'files')) {
                $last = \$entry->{formats}->{$format}->{$fld};
            } else {
                goto PARSE_ERROR;
            }

            $$last = $val if (defined $last)

        } elsif (/^\s+/ and defined($last)) {
            $$last .= "\n$_";
        } else {
            goto PARSE_ERROR;
        }
    }

    close DOCFILE;

    return $entry;


PARSE_ERROR:
    &$ErrorProc($file, "Parse error at line $line");
    close DOCFILE;
    return undef;
}


sub DwwwSection2Section {
    my $entry = shift;

    my $sec   = $entry->{'dwww-section'} if defined $entry->{'dwww-section'};
    my $title = defined $entry->{'dwww-title'} ? $entry->{'dwww-title'} :
                    defined $entry->{'title'} ? $entry->{'title'} : undef;

    return unless defined $sec and defined $title;

    if (length($sec) > length($title) &&
         substr ($sec, -length($title))  eq $title) {
            $sec = substr ($sec, 0, -length($title));
    } else {
            return;
    }

    $sec =~ s|^/+||;
    $sec =~ s|/+$||;
    $entry->{'section'} = $sec;
}


1;
