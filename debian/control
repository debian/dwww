Source: dwww
Section: doc
Priority: optional
Maintainer: Robert Luberda <robert@debian.org>
Build-Depends: debhelper-compat (= 13), dh-apache2, lsb-release, publib-dev
Build-Conflicts: apache2-dev (<< 2.4.4-6~)
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/dwww.git
Vcs-Browser: https://salsa.debian.org/debian/dwww

Package: dwww
Architecture: any
Depends: apache2 | httpd-cgi,
         debianutils (>= 2.5),
         doc-base (>= 0.8.12),
         file,
         libfile-ncopy-perl,
         libmime-types-perl (>= 1.15),
         man-db (>> 2.5.2),
         sensible-utils,
         ucf (>= 3.12),
         ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Breaks: apache2 (<< 2.4.4-6~)
Recommends: apt, dlocate (>= 0.5-0.1), info2www, swish++, ${misc:Recommends}
Suggests: doc-debian, dpkg-www, links | www-browser
Description: Read all on-line documentation with a WWW browser
 All installed on-line documentation will be served via a local HTTP
 server at http://localhost/dwww/. This package runs cron scripts to
 convert available resources to the HTML pages.  Executing the dwww
 command starts a sensible WWW browser locally to access them.
 .
 You need to install a CGI-capable HTTP server and enable its CGI script
 capability manually (CGI may be disabled by default for the security
 consideration).  For apache2, do something along the following:
 .
  $ sudo a2enmod cgid
  $ sudo systemctl restart apache2
 .
 The default for the HTTP/CGI access is limited to the local user for
 the security consideration.
